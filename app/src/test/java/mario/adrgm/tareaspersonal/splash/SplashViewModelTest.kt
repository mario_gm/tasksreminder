package mario.adrgm.tareaspersonal.splash

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class SplashViewModelTest {
    private lateinit var splashViewModel: SplashViewModel
    @Mock
    private lateinit var mockContext: Context
    @Mock
    private lateinit var mockConnectivityManager: ConnectivityManager
    @Mock
    private lateinit var mockNetwork: Network
    @Mock
    private lateinit var mockNetworkCapabilities: NetworkCapabilities

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        splashViewModel = SplashViewModel()
        `when`(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager)
    }

    @Test
    fun thereIsNotNetwork() {
        `when`(mockConnectivityManager.activeNetwork).thenReturn(null)
        val isAvailable = splashViewModel.isInternetConnectionAvailable(mockContext)
        assert(!isAvailable)
    }

    @Test
    fun thereIsNotConnection() {
        `when`(mockConnectivityManager.activeNetwork).thenReturn(mockNetwork)
        `when`(mockConnectivityManager.getNetworkCapabilities(mockNetwork)).thenReturn(mockNetworkCapabilities)
        `when`(mockNetworkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)).thenReturn(false)

        val isAvailable = splashViewModel.isInternetConnectionAvailable(mockContext)

        assert(!isAvailable)
    }

    @Test
    fun thereIsConnection() {
        `when`(mockConnectivityManager.activeNetwork).thenReturn(mockNetwork)
        `when`(mockConnectivityManager.getNetworkCapabilities(mockNetwork)).thenReturn(mockNetworkCapabilities)
        `when`(mockNetworkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)).thenReturn(true)

        val isAvailable = splashViewModel.isInternetConnectionAvailable(mockContext)

        assert(isAvailable)
    }
}
