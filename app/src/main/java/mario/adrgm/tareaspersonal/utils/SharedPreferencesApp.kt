package mario.adrgm.tareaspersonal.utils

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class SharedPreferencesApp(val context: Context) {
    private var sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun writeStringData(tag: String, data: String) {
        sharedPreferences.edit {
            this.putString(tag, data)
            this.apply()
        }
    }

    fun getData(tag: String): Any? {
        return sharedPreferences.getString(tag, Constants.NO_DATA)
    }

    fun deleteData(tag: String) {
        sharedPreferences.edit {
            this.remove(tag)
        }
    }
}