package mario.adrgm.tareaspersonal.utils

object Constants {

    const val TAG_LOGIN = "Login"
    const val USER = "User"
    const val NO_DATA = "No Information"
    const val TAG_GOOGLE_AUTHENTICATION = "Firebase_Google"
}