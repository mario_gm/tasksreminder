package mario.adrgm.tareaspersonal.welcome

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import mario.adrgm.tareaspersonal.MainActivity
import mario.adrgm.tareaspersonal.R
import mario.adrgm.tareaspersonal.databinding.ActivityWelcomeBinding
import mario.adrgm.tareaspersonal.login.LoginActivity
import mario.adrgm.tareaspersonal.utils.Constants.USER

class WelcomeActivity : MainActivity() {

    private lateinit var welcomeBinding: ActivityWelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        welcomeBinding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(welcomeBinding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        welcomeBinding.tvGreeting.text = getString(R.string.greeting_user, intent.getStringExtra(USER)?.substringBefore("@") ?: "")
        initDrawer()
    }

    private fun initDrawer() {
        val drawerToggle = ActionBarDrawerToggle(
            this,
            welcomeBinding.drawerLayout,
            R.string.drawer_open,
            R.string.drawer_close
        )
        welcomeBinding.drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        // Configuración del NavigationView
        welcomeBinding.navigationView.setNavigationItemSelectedListener { menuItem ->
            handleDrawerMenuItemClick(menuItem)
            true
        }
    }

    private fun handleDrawerMenuItemClick(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.expired_menu -> {

            }
            R.id.tasks_menu -> {

            }
            R.id.configuration_menu -> {

            }
            R.id.profile_menu -> {

            }
            R.id.logout -> {
                logOut()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (welcomeBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    welcomeBinding.drawerLayout.closeDrawer(GravityCompat.START)
                } else {
                    welcomeBinding.drawerLayout.openDrawer(GravityCompat.START)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        // Implementar según sea necesario
    }

    override fun hideProgress() {
        // Implementar según sea necesario
    }

    private fun logOut() {
        deleteData(USER)
        Firebase.auth.signOut()
        changeActivity(LoginActivity::class.java, null)
    }

    override fun onBackPressed() {
        if (welcomeBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            welcomeBinding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
