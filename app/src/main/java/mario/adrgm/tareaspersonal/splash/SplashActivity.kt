package mario.adrgm.tareaspersonal.splash

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import mario.adrgm.tareaspersonal.MainActivity
import mario.adrgm.tareaspersonal.R
import mario.adrgm.tareaspersonal.databinding.ActivitySplashBinding
import mario.adrgm.tareaspersonal.login.LoginActivity

class SplashActivity : MainActivity() {

    private lateinit var activityBinding: ActivitySplashBinding
    private val splashViewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBinding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(activityBinding.root)
    }

    companion object {
        var count = 0
    }

    override fun onResume() {
        super.onResume()
        if (count < 1) {
            initProgressBar()
        }
        count += 1
    }

    override fun onDestroy() {
        super.onDestroy()
        count = 0
    }

    override fun showProgress() {
        activityBinding.progressSplash.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        activityBinding.progressSplash.visibility = View.GONE
    }
    private fun initProgressBar() {
        showProgress()
        if (splashViewModel.isInternetConnectionAvailable(this)) {
            hideProgress()
            changeActivity(LoginActivity::class.java, null)
        } else {
            showToast(getString(R.string.no_connection), false)
            hideProgress()
        }
    }

}