package mario.adrgm.tareaspersonal.splash

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import mario.adrgm.tareaspersonal.BaseViewModel

class SplashViewModel: BaseViewModel() {
    fun isInternetConnectionAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // Tests if device is connected to some network
        val activeNetwork = connectivityManager.activeNetwork ?: return false

        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)

        // Verifies if the network has Internet capabilities
        return networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
    }
}