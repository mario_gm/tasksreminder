package mario.adrgm.tareaspersonal.login

import android.content.Context
import android.os.Bundle
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.Identity
import mario.adrgm.tareaspersonal.MainActivity
import mario.adrgm.tareaspersonal.R
import mario.adrgm.tareaspersonal.databinding.FragmentRegisterBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _bindingRegister: FragmentRegisterBinding? = null
    private val bindingRegister get() = _bindingRegister!!
    private val MASK_PASSOWRD = 129

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindingRegister.editTextPassword.inputType = 129
        setListeners()
    }

    private fun setListeners() {
        bindingRegister.btnRegisterGoogle.setOnClickListener(this)
        bindingRegister.btnRegister.setOnClickListener(this)
        bindingRegister.btnShowPassword.setOnClickListener(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _bindingRegister = FragmentRegisterBinding.inflate(inflater, container, false)
        return bindingRegister.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegisterFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        configureOneTapForNewUser()
    }

    private fun configureOneTapForNewUser() {
        (requireActivity() as LoginActivity).oneTapClient = Identity.getSignInClient(requireActivity())
        (requireActivity() as LoginActivity).signInRequest = BeginSignInRequest.builder()
            .setPasswordRequestOptions(
                BeginSignInRequest.PasswordRequestOptions.builder()
                .setSupported(true)
                .build())
            .setGoogleIdTokenRequestOptions(
                BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                    .setSupported(true)
                    // Your server's client ID, not your Android client ID.
                    .setServerClientId(getString(R.string.your_web_client_id))
                    .setFilterByAuthorizedAccounts(false)
                    .build())
            // Automatically sign in when exactly one credential is retrieved.
            .setAutoSelectEnabled(true)
            .build()
    }

    private fun validateEmail(input: String, inputConfirm: String): Boolean {
        val emailRegex = Regex("^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$")
        return emailRegex.matches(input) && input == inputConfirm
    }

    private fun validatePassword(input: String, inputConfirm: String): Boolean {
        val passwordRegex = Regex("^[a-zA-Z0-9]+$")
        return passwordRegex.matches(input) && input == inputConfirm
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            bindingRegister.btnRegisterGoogle.id -> {
                (requireActivity() as LoginActivity).showUIGoogle()
            }
            bindingRegister.btnRegister.id -> {
                val email = bindingRegister.editTextEmail.text.toString()
                val password = bindingRegister.editTextPassword.text.toString()
                val verifyEmail = validateEmail(email,
                    bindingRegister.editTextConfirmEmail.text.toString())
                val verifyPassword = validatePassword(password,
                    bindingRegister.editTextConfirmPassword.text.toString())
                if (verifyEmail && verifyPassword) {
                    (requireActivity() as LoginActivity).createNewUser(email, password)
                } else {
                    (requireActivity() as MainActivity).showToast(getString(R.string.not_valid_email_password),
                        short = true)
                }
            }
            bindingRegister.btnShowPassword.id -> {
                val currentInputType = bindingRegister.editTextPassword.inputType
                if (currentInputType == MASK_PASSOWRD) {
                    bindingRegister.editTextPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                } else {
                    bindingRegister.editTextPassword.inputType = MASK_PASSOWRD
                }
            }
        }
    }
}