package mario.adrgm.tareaspersonal

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import mario.adrgm.tareaspersonal.databinding.ActivityMainBinding
import mario.adrgm.tareaspersonal.utils.SharedPreferencesApp

abstract class MainActivity : AppCompatActivity() {

    private lateinit var activityBinding: ActivityMainBinding
    private lateinit var sharedPreferencesApp: SharedPreferencesApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initSharedPreferences()
        activityBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityBinding.root)
    }
    fun changeActivity(activity: Class<*>, bundle: Bundle?) {
        val intentToNewActivity = Intent(this, activity)
        if (bundle != null)
            intentToNewActivity.putExtras(bundle)
        startActivity(intentToNewActivity)
    }

    fun showToast(message: String, short: Boolean) {
        Toast.makeText(this, message, if (short) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    private fun initSharedPreferences() {
        sharedPreferencesApp = SharedPreferencesApp(this)
    }

    fun writeStringData(tag: String, data: String) {
        sharedPreferencesApp.writeStringData(tag, data)
    }

    fun getData(tag: String): Any? {
        return sharedPreferencesApp.getData(tag)
    }

    fun deleteData(tag: String){
        sharedPreferencesApp.deleteData(tag)
    }

    abstract fun showProgress()

    abstract fun hideProgress()

}